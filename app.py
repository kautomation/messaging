from socketIO_client import SocketIO, LoggingNamespace
import time, json, os, threading, requests

host = 'http://sandbox.nparking.net' 
url_health = 'https://nparking.net/api/terminal/health'
url_in_command = 'https://nparking.net/api/terminal/button/in'
headers = {'Content-type': 'application/json'}

rfid_no = '1'
direction = 'in'
side = ''

def on_connect():
    print('connect')

def on_disconnect():
    print('disconnect')

def on_reconnect():
    print('reconnect')

def on_server_response(*args):
    #socketIO.emit('message', time.time())
    #print(args)
    #print('message is thissss: ', args[0])
    command = str(args[0].decode())[1:]
    print(args[0])
    d = json.loads(command)
    try:
        print(d[1]['data']['barrier_no'])
        print(d[1]['data']['open_it'])
        if ((d[1]['data']['open_it'] == 'yes') and (d[1]['data']['barrier_no'] == rfid_no)) :
            os.popen('sudo /home/debian/messaging/GPIO in open; sleep 2; sudo /home/debian/messaging/GPIO in close')
    except:
        pass
#    if d[1]['data'] == 'off':
#        os.popen('sudo /home/debian/airportrfidreader/GPIO in close')

def read():
    while True:
        time.sleep(1)
        rfids=os.popen("/home/debian/messaging/read tmr://0.0.0.0 --ant 1")
        for rfid in rfids:
            #print(rfid.rstrip('\n'))
            if rfid.startswith('05'):
                print(rfid.rstrip('\n'))
                socketIO.emit('rfid_broadcast_event', {'data' :{'rfid_id':rfid.rstrip('\n'), 'rfid_no':rfid_no, 'direction': direction }})

def check():
    while True:
        time.sleep(5)
        try:
            print('#out from far command#')
            data_in_dis = json.dumps({'terminal_no': rfid_no})
            response_in_dis = requests.post(url_in_command, data=data_in_dis, headers=headers, verify=False, timeout=4)
            data_in_dis = json.loads(response_in_dis.text)
            print(data_in_dis)
            print(data_in_dis['msg'])
            if data_in_dis['msg'] == 'open barier':
                print('open barier now')
                os.popen('sudo /home/debian/messaging/GPIO in open; sleep 3; sudo /home/debian/messaging/GPIO in close')
        except Exception as e:
            print(e)

def health():
    while True:
        time.sleep(5)

        try:
            data_health = json.dumps({'terminal_no': rfid_no + direction + side})
            health_msg = requests.post(url_health, data=data_health, headers=headers, verify=False, timeout=1)
            print(health_msg.text)
        except:
            print("error HEALTH request")




socketIO = SocketIO(host=host)
socketIO.on('connect', on_connect)
socketIO.on('disconnect', on_disconnect)
socketIO.on('reconnect', on_reconnect)
socketIO.on('message', on_server_response)
x1 = threading.Thread(target=read)
x2 = threading.Thread(target=check)
x3  = threading.Thread(target=health)
x1.start()
x2.start()
x3.start()


socketIO.wait()